#!/bin/bash
set -e

# Create mosquitto passwordfile it it doesn't alread exist.
if ! [[ -e /mosquitto/passwd/passwordfile ]]; then
  if ( [ -z "${MOSQUITTO_USERNAME}" ] || [ -z "${MOSQUITTO_PASSWORD}" ] ); then
    echo "MOSQUITTO_USERNAME or MOSQUITTO_PASSWORD not defined"
    exit 1
  fi
  touch /mosquitto/passwd/passwordfile
  mosquitto_passwd -b /mosquitto/passwd/passwordfile
fi

exec "$@"